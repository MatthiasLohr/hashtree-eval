# HashTree Evaluation

Evaluation code for our method for structure-aware hashing of hierarchically structured data, as presented in

> Lohr, M., Hund, J., Jürjens, J., & Staab, S. (2019, July). Ensuring genuineness for selectively disclosed confidential data using distributed ledgers: Applications to rail wayside monitoring. In 2019 IEEE International Conference on Blockchain (Blockchain) (pp. 477-482). IEEE.

Paper link: https://ieeexplore.ieee.org/abstract/document/8946253


## Usage

## License (GPLv3)

Roundcube Helm Chart - Copyright (C) 2021 [Matthias Lohr](https://mlohr.com/) &lt;[mail@mlohr.com](mailto:mail@mlohr.com)&gt;

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
