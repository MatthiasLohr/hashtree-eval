#!/usr/bin/env python3

import csv
import hashlib
import sys
from argparse import ArgumentParser
from os import stat
from statistics import mean, stdev
from time import time

from hash_classic import compute_hash as compute_hash_classic
from hash_tree import compute_hash as compute_hash_tree


def main() -> int:
    argument_parser = ArgumentParser()
    argument_parser.add_argument("-a", "--algorithm", choices=hashlib.algorithms_available, default="sha512")
    argument_parser.add_argument("-i", "--iterations", type=int, default=1)
    argument_parser.add_argument("files", nargs="+")
    args = argument_parser.parse_args()

    csv_writer = csv.writer(sys.stdout)

    measurements = {}

    for filename in args.files:
        # "preheat" (try to get file loaded into RAM buffers"
        compute_hash_classic(filename, args.algorithm)
        compute_hash_tree(filename, args.algorithm)

        for method in [compute_hash_classic, compute_hash_tree]:
            method_measurements = []

            for iteration in range(args.iterations):
                time_start = time()
                _ = method(filename, args.algorithm)
                time_end = time()
                method_measurements.append((time_end - time_start) * 1000)

            measurements.update({method: method_measurements})

        csv_writer.writerow(
            [
                filename,
                stat(filename).st_size,
                mean(measurements[compute_hash_classic]),
                stdev(measurements[compute_hash_classic]) if args.iterations > 1 else 0,
                mean(measurements[compute_hash_tree]),
                stdev(measurements[compute_hash_tree]) if args.iterations > 1 else 0,
            ]
        )
        sys.stdout.flush()

    return 0


if __name__ == "__main__":
    sys.exit(main())
