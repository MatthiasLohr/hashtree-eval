#!/usr/bin/env python3

import hashlib
import sys
from argparse import ArgumentParser


def main() -> int:
    argument_parser = ArgumentParser()
    argument_parser.add_argument("-a", "--algorithm", choices=hashlib.algorithms_available, default="sha512")
    argument_parser.add_argument("--buffer-size", type=int, default=65536)
    argument_parser.add_argument("filename")
    args = argument_parser.parse_args()

    hash_value = compute_hash(args.filename, args.algorithm, args.buffer_size)
    print(hash_value)

    return 0


def compute_hash(filename: str, algorithm: str, buffer_size: int = 65536) -> str:
    hashlib_instance = hashlib.new(algorithm)
    with open(filename, "rb") as fp:
        while True:
            chunk = fp.read(buffer_size)
            if not chunk:
                break
            hashlib_instance.update(chunk)

    return hashlib_instance.hexdigest()


if __name__ == "__main__":
    sys.exit(main())
