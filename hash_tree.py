#!/usr/bin/env python3

from __future__ import annotations

import hashlib
import sys
from argparse import ArgumentParser
from typing import List

from lxml.etree import XMLParser
from lxml.etree import parse as parse_xml


def main() -> int:
    argument_parser = ArgumentParser()
    argument_parser.add_argument("-a", "--algorithm", choices=hashlib.algorithms_available, default="sha512")
    argument_parser.add_argument("filename")
    args = argument_parser.parse_args()

    hash_value = compute_hash(args.filename, args.algorithm)
    print(hash_value)

    return 0


def compute_hash(filename: str, algorithm: str) -> str:
    parser_target = HashTreeParserTarget(algorithm=algorithm)
    parser = XMLParser(target=parser_target)
    result = parse_xml(filename, parser=parser)
    return result.hex()


class HashTreeParserTarget(object):
    """
    https://lxml.de/parsing.html#the-target-parser-interface
    """

    def __init__(self, algorithm: str) -> None:
        self._algorithm = algorithm

        self._stack: List[List[bytes]] = [[]]

    def _hash(self, content: bytes) -> bytes:
        hashlib_instance = hashlib.new(self._algorithm)
        hashlib_instance.update(content)
        return hashlib_instance.digest()

    def _append_hash_to_stack(self, content: bytes | str):
        if isinstance(content, str):
            content = content.encode("utf-8")

        self._stack[-1].append(self._hash(content))

    def start(self, tag_name, attributes) -> None:
        self._stack.append([])  # create new stack layer
        self._append_hash_to_stack(tag_name)

        for attribute_name, attribute_value in attributes.items():
            self._append_hash_to_stack(attribute_name + attribute_value)

    def data(self, data) -> None:
        self._append_hash_to_stack(data)

    def comment(self, text) -> None:
        self._append_hash_to_stack(text)

    def end(self, tag_name) -> None:
        children_hash_list = self._stack.pop(-1)

        hashlib_instance = hashlib.new(self._algorithm)
        for hash_value in children_hash_list:
            hashlib_instance.update(hash_value)

        self._append_hash_to_stack(hashlib_instance.digest())

    def close(self) -> bytes:
        assert len(self._stack) == 1

        hashlib_instance = hashlib.new(self._algorithm)
        for hash_value in self._stack[0]:
            hashlib_instance.update(hash_value)

        return hashlib_instance.digest()


if __name__ == "__main__":
    sys.exit(main())
